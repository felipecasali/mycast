#!/bin/ksh

sample=`ctm config server:hostgroup:agents::get controlm ForecastApp`

for row in $(echo "${sample}" | jq -r '.[] | @base64'); do
    _jq() {
     echo ${row} | base64 --decode | jq -r ${1}
    }

   ctm config server:hostgroup:agent::delete controlm ForecastApp `echo $(_jq '.host')`
   ctm config server:agent::delete controlm `echo $(_jq '.host')`
done