#!/bin/bash
yum update -y
yum install -y jq
yum install -y ksh
yum install -y wget
yum install -y psmisc
sudo curl --silent --location https://rpm.nodesource.com/setup_14.x | sudo bash -
sudo yum install -y nodejs
sudo yum install -y java-1.8.0
wget https://ec2-54-207-171-59.sa-east-1.compute.amazonaws.com:8443/automation-api/ctm-cli.tgz --no-check-certificate
sudo npm install -g ctm-cli.tgz
sudo useradd ec2-user
runuser -l ec2-user -c 'ctm env add ctm "https://ec2-54-207-171-59.sa-east-1.compute.amazonaws.com:8443/automation-api" i2t_fcasali i2t_fcasali'
runuser -l ec2-user -c 'ctm env set ctm'
sudo cp -r /home/ec2-user/.ctm /root
runuser -l ec2-user -c 'ctm provision image Agent_20.Linux'
echo "54.207.171.59 controlm" >> /etc/hosts
runuser -l ec2-user -c 'ctm provision agent::setup controlm `curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip`'
sleep 10
runuser -l ec2-user -c 'ctm config server:hostgroup:agent::add controlm ForecastApp `curl -H "Metadata-Flavor: Google" http://169.254.169.254/computeMetadata/v1/instance/network-interfaces/0/access-configs/0/external-ip`'
