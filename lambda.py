from botocore.vendored import requests
import json

def lambda_handler(event, context):
    
    url = "https://54.207.171.59:8443/automation-api/session/login"
    
    payload="{\"username\": \"fcasali\", \"password\": \"PreSales2021@\"}"
    headers = {
      'Content-Type': 'application/json',
      'Cookie': 'SameSite=strict; JSESSIONID=F864173B0753D3E44D7E671B18333900'
    }
    
    response = requests.request("POST", url,  verify=False, headers=headers, data=payload)
    
    print(response.text)
    
    x = response.text
    y = json.loads(x)
    token = print(y["token"])
    
    print(token)
    
    url = "https://54.207.171.59:8443/automation-api/run/order"
    
    payload="{\"ctm\": \"controlm\", \"folder\": \"FCA_I2T_MyCast_DataPipeline\"}"

    headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + str(token) + " ' " ,
      'Cookie': 'SameSite=strict; JSESSIONID=F864173B0753D3E44D7E671B18333900'
    }
    
    response = requests.request("POST", url,  verify=False, headers=headers, data=payload)
    
    print(response.text)

    return {
        #'statusCode': 200,
        #'body': json.dumps('Hello from Lambda!')
    }