#!/bin/ksh -x

COUNTRY=$1
CITY=$2
CUSTOMER_EMAIL=$3

PREVISAO=`curl -s "http://api.openweathermap.org/data/2.5/weather?q=$CITY,$COUNTRY&appid=9c7f7ff74bd4aab0997877fb514ee68a&units=metric&lang=en_us" | jq -r .weather[].description | sed 's/"//g' | sed 's/,//g'`
MIN=`curl -s "http://api.openweathermap.org/data/2.5/weather?q=$CITY,$COUNTRY&appid=9c7f7ff74bd4aab0997877fb514ee68a&units=metric&lang=en_us" | jq -r .main.temp_min | sed 's/"//g' | sed 's/,//g'`
MAX=`curl -s "http://api.openweathermap.org/data/2.5/weather?q=$CITY,$COUNTRY&appid=9c7f7ff74bd4aab0997877fb514ee68a&units=metric&lang=en_us" | jq -r .main.temp_max | sed 's/"//g' | sed 's/,//g'`

echo "$PREVISAO" "$MIN" "$MAX"

if [ "$MIN" -lt  10 ]; then
        MESSAGE="PREPARE YOUR MOST HOT CLOTHES. ITS GOING TO BE A VERY COLD DAY IN $CITY - $COUNTRY"
else
        if [ "$MAX" -gt 30 ]; then
                MESSAGE="TODAY YOU WILL NEED MORE WATER AND LIGHT CLOTHES IN $CITY - $COUNTRY"
        else
                MESSAGE="IT WILL BE A NICE DAY IN $CITY - $COUNTRY"
        fi
fi

# Send email with customized forescast
echo "CUSTOMER E-MAIL:" $CUSTOMER_EMAIL
echo "MESSAGE:" $MESSAGE

exit 0